﻿using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using MovieBase.Service.Controllers;
using MovieBase.Domain.Contracts.Services;
using MovieBase.Domain.Models;
using Xunit;

namespace MovieBase.Service.Tests.ControllerTests
{
    public class MoviesControllerTests
    {
        [Fact]
        public async void AddMovie_ForValidData_Returns200()
        {
            // Given
            string title = "Harry Potter";
            string genere = "Action";
            NewMovieModel newMovie = new NewMovieModel {Title = title, Genere = genere};
            var movieService = A.Fake<IMovieService>();
            A.CallTo(() => movieService.AddMovieAsync(newMovie)).Returns(null);
            var moviesController = new MoviesController(movieService);

            // When
            var actionResult = await moviesController.AddMovie(title, genere);
            // Then
            Assert.IsType<OkResult>(actionResult);
        }
    }
}
