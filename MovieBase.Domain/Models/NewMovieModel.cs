﻿using System.ComponentModel.DataAnnotations;

namespace MovieBase.Domain.Models
{
    public class NewMovieModel
    {
        [Required]
        public string Title { get; set; }
        public string Genere { get; set; }
    }
}
