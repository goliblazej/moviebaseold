﻿using System.ComponentModel.DataAnnotations;

namespace MovieBase.Domain.Entities
{
    public class Rate
    {
        [Range(1, int.MaxValue)]
        public int MovieId { get; set; }
        [Range(0, int.MaxValue)]
        public int UserId { get; set; }
        [Range(1, 5)]
        public int Score { get; set; }
    }
}
