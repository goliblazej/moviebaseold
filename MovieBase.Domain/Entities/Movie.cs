﻿using System.ComponentModel.DataAnnotations;

namespace MovieBase.Domain.Entities
{
    public class Movie
    {
        [Range(1, int.MaxValue)]
        public int MovieId { get; set; }

        [Required]
        public string Title { get; set; }

        public string Genere { get; set; }

        [Range(0, 5.0)]
        public decimal AverageScore { get; set; }
    }
}
