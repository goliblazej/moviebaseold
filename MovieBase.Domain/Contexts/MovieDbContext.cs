﻿using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Entities;

namespace MovieBase.Domain.Contexts
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Movie>().HasKey(m => new { m.MovieId });
        }
    }
}
