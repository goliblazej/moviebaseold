﻿using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Entities;

namespace MovieBase.Domain.Contexts
{
    public class UserDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public UserDbContext(DbContextOptions<UserDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasKey(m => new { m.UserId });
        }
    }
}

