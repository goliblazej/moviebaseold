﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Contracts.Services;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _movieRepository.GetMoviesAsync();
        }

        public async Task<Movie> GetMovieAsync(int movieId)
        {
            return await _movieRepository.GetMovieAsync(movieId);
        }

        public async Task AddMovieAsync(NewMovieModel movie)
        {
            await _movieRepository.InsertMovieAsync(movie);
        }

        public async Task DeleteMovieAsync(int movieId)
        {
            await _movieRepository.DeleteMovieAsync(movieId);
        }
    }
}
