﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Contracts.Services;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> GetUserAsync(int userId)
        {
            return await _userRepository.GetUserAsync(userId);
        }

        public async Task AddUserAsync(NewUserModel user)
        {
            await _userRepository.InsertUserAsync(user);
        }

        public async Task DeleteUserAsync(int userId)
        {
            await _userRepository.DeleteUserAsync(userId);
        }
    }
}

