﻿using System;
using System.Threading.Tasks;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts.Repositories
{
    public interface IUserRepository : IDisposable
    {
        Task<User> GetUserAsync(int userId);
        Task InsertUserAsync(NewUserModel user);
        Task DeleteUserAsync(int userId);
    }
}
