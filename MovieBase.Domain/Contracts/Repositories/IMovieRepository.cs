﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts.Repositories
{
    public interface IMovieRepository : IDisposable
    {
        Task<IEnumerable<Movie>> GetMoviesAsync();
        Task<Movie> GetMovieAsync(int movieId);
        Task InsertMovieAsync(NewMovieModel movie);
        Task DeleteMovieAsync(int movieId);
    }
}