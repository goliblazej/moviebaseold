﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts.Services
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetMoviesAsync();
        Task<Movie> GetMovieAsync(int movieId);
        Task AddMovieAsync(NewMovieModel movie);
        Task DeleteMovieAsync(int movieId);
    }
}
