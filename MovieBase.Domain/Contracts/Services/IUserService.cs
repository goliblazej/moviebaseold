﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts.Services
{
    public interface IUserService
    {
        Task<User> GetUserAsync(int userId);
        Task AddUserAsync(NewUserModel user);
        Task DeleteUserAsync(int userId);
    }
}
