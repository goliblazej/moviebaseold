﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Contexts;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDbContext _context;

        public MovieRepository(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<Movie> GetMovieAsync(int movieId)
        {
            return await _context.Movies.Where(m => m.MovieId == movieId).FirstOrDefaultAsync();
        }

        public async Task InsertMovieAsync(NewMovieModel movie)
        {
            await _context.Movies.AddAsync(new Movie
            {
                MovieId = await GetNewAvailableId(),
                Title = movie.Title,
                Genere = movie.Genere,
                AverageScore = 0.0m
            });
            await _context.SaveChangesAsync();
        }

        private async Task<int> GetNewAvailableId()
        {
            var movieMaxId = await _context.Movies.OrderByDescending(m => m.MovieId).FirstOrDefaultAsync();

            if (movieMaxId == null)
                return 1;

            return movieMaxId.MovieId + 1;
        }

        public async Task DeleteMovieAsync(int movieId)
        {
            var movieToRemove =  _context.Movies.Find(movieId);
            if (movieToRemove != null)
            {
                _context.Movies.Remove(movieToRemove);
                await _context.SaveChangesAsync();
            }
        }

        #region IDisposable

        private bool _disposed;

        ~MovieRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion
    }
}
