﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Contexts;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Repositories
{ 
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext _context;

        public UserRepository(UserDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserAsync(int userId)
        {
            return await _context.Users.Where(m => m.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task InsertUserAsync(NewUserModel user)
        {
            await _context.Users.AddAsync(new User
            {
                UserId= await GetNewAvailableId(),
                Login = user.Login,
                Name = user.Name,
                Surname = user.Surname
            });
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(int userId)
        {
            var userToRemove = _context.Users.Find(userId);
            if (userToRemove != null)
            {
                _context.Users.Remove(userToRemove);
                await _context.SaveChangesAsync();
            }
        }

        private async Task<int> GetNewAvailableId()
        {
            var userMaxId = await _context.Users.OrderByDescending(u => u.UserId).FirstOrDefaultAsync();

            if (userMaxId == null)
                return 1;

            return userMaxId.UserId + 1;
        }

        #region IDisposable

        private bool _disposed;

        ~UserRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion
    }
}
