﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Contexts;
using Xunit;
using MovieBase.Domain.Models;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Repositories;

namespace MovieBase.Domain.Tests.RepositoryTests
{
    public class MovieRepositoryTests
    {
        private readonly DbContextOptions<MovieDbContext> _options;

        public MovieRepositoryTests()
        {
            _options = new DbContextOptionsBuilder<MovieDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
        }

        [Fact]
        public async void GetMoviesAsync_ForNoData_ReturnsEmptyListOfItems()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var items = await movieRepository.GetMoviesAsync();

                // Then
                Assert.NotNull(items);
                Assert.Empty(items);
            }
        }

        [Fact]
        public async void GetMoviesAsync_DataExsist_ReturnsListOfItems()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync();

                // Then
                Assert.NotNull(movies);
                Assert.NotEmpty(movies);
                Assert.Equal(3, new List<Movie>(movies).Count);
            }
        }

        [Fact]
        public async void GetMovieAsync_MovieExsist_ReturnsMovie()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movie = await movieRepository.GetMovieAsync(2);

                // Then
                Assert.NotNull(movie);
                Assert.Equal(2, movie.MovieId);
                Assert.Equal("Movie2", movie.Title);
                Assert.Equal("Family", movie.Genere);
                Assert.Equal(3.47m, movie.AverageScore);
            }
        }

        [Fact]
        public async void GetMovieAsync_MovieDoesntExsist_ReturnsEmpty()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movie = await movieRepository.GetMovieAsync(7);

                // Then
                Assert.Null(movie);
            }
        }

        [Fact]
        public async void InsertMovieAsync_ForNewMovie_InsertNewMovie()
        {
            // Given
            var movie = new NewMovieModel { Title = "Harry Potter", Genere = "Action"};

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleAsync(i => i.Title == "Harry Potter");
                Assert.NotNull(newMovie);
                Assert.Equal("Harry Potter", newMovie.Title);
                Assert.Equal("Action", newMovie.Genere);
            }
        }

        [Fact]
        public async void InsertMovieAsync_ForTitleMissing_DontInsertNewMovie()
        {
            // Given
            var movie = new NewMovieModel { Title = "", Genere = "Action" };

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleOrDefaultAsync(i => i.Title == "Harry Potter");
                Assert.Null(newMovie);
            }
        }

        [Fact]
        public async void InsertMovieAsync_ForGenereMissing_DontInsertNewMovie()
        {
            // Given
            var movie = new NewMovieModel { Title = "Harry Potter", Genere = "" };

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleOrDefaultAsync(i => i.Genere == "Action");
                Assert.Null(newMovie);
            }
        }

        [Fact]
        public async void DeleteMovieAsync_MovieIdExist_DeleteMovie()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.DeleteMovieAsync(1);

                // Then
                var moviesCount = context.Movies.Count();
                Assert.Equal(2, moviesCount);
            }
        }

        [Fact]
        public async void DeleteMovieAsync_MovieIdDoesntExist_MovieNotDeleted()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.DeleteMovieAsync(6);

                // Then
                var moviesCount = context.Movies.Count();
                Assert.Equal(3, moviesCount);
            }
        }

        private void BuildMovieContext(MovieDbContext context)
        {
            context.Movies.Add(new Movie { MovieId = 1, Title = "Movie1", Genere = "Action", AverageScore = 2.98m });
            context.Movies.Add(new Movie { MovieId = 2, Title = "Movie2", Genere = "Family", AverageScore = 3.47m });
            context.Movies.Add(new Movie { MovieId = 3, Title = "Movie3", Genere = "Comedy", AverageScore = 4.11m });
        }
    }
}