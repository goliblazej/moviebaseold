﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Models;
using Xunit;

namespace MovieBase.Domain.Tests.ModelTests
{
    public class NewUserModelTests
    {
        [Theory]
        [InlineData("", false)] // Invalid login
        [InlineData("login", true)] // Valid login
        public void NewUserModel_ForValues_ValidatesCorrectly(string login, bool expectedResult)
        {
            // Given
            var newUserModel = new NewUserModel
            {
                Login = login,
                Name = "",
                Surname = ""
            };
            var validationContext = new ValidationContext(newUserModel);

            // When
            var result = Validator.TryValidateObject(newUserModel, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}
