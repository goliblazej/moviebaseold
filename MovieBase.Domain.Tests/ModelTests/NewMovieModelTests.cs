﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Models;
using Xunit;

namespace MovieBase.Domain.Tests.ModelTests
{
    public class NewMovieModelTests
    {
        [Theory]
        [InlineData("", false)] // Invalid title
        [InlineData("title", true)] // Valid title
        public void NewMovieModel_ForValues_ValidatesCorrectly(string title, bool expectedResult)
        {
            // Given
            var newMovieModel = new NewMovieModel
            {
                Title = title,
                Genere = ""
            };
            var validationContext = new ValidationContext(newMovieModel);

            // When
            var result = Validator.TryValidateObject(newMovieModel, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}
