﻿using FakeItEasy;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MovieBase.Domain.Tests.ServiceTests
{
    public class MovieServiceTests
    {
        [Fact]
        public async void GetMoviesAsync_ForMovies_ReturnsAllMovies()
        {
            // Given
            var movies = new List<Movie>
            {
                new Movie { MovieId = 1, Title = "Harry Potter", Genere = "Action", AverageScore = 3.22m },
                new Movie { MovieId = 2, Title = "Harry Potter 2", Genere = "Family", AverageScore = 4.11m }
            };
            var movieRepository = A.Fake<IMovieRepository>();
            A.CallTo(() => movieRepository.GetMoviesAsync()).Returns(movies);
            var movieService = new MovieService(movieRepository);

            // When
            var result = await movieService.GetMoviesAsync();

            // Then
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async void GetMovieAsync_ForMovie_ReturnsMovie()
        {
            // Given
            var movie = new Movie { MovieId = 2, Title = "Harry Potter 2", Genere = "Family", AverageScore = 4.11m };
            var movieRepository = A.Fake<IMovieRepository>();
            A.CallTo(() => movieRepository.GetMovieAsync(2)).Returns(movie);
            var movieService = new MovieService(movieRepository);

            // When
            var result = await movieService.GetMovieAsync(2);

            // Then
            Assert.Equal(2, result.MovieId);
            Assert.Equal("Harry Potter 2", result.Title);
            Assert.Equal("Family", result.Genere);
            Assert.Equal(4.11m, result.AverageScore);
        }

        
    }
}
