﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Entities;
using Xunit;

namespace MovieBase.Domain.Tests.EntityTests
{
    public class MovieTests
    {
        [Theory]
        [InlineData(0, "", 2.0, false)] //Invalid MovieId, Invalid title, Valid averageScore
        [InlineData(0, "title", 2.0, false)] //Invalid MovieId, Valid title, Valid averageScore
        [InlineData(1, "", 2.0, false)] //Valid MovieId, Invalid title, Valid averageScore
        [InlineData(1, "title", -1.0, false)] //Valid MovieId, Valid title, Invalid averageScore
        [InlineData(1, "title", 5.1, false)] //Valid MovieId, Valid title, Invalid averageScore
        [InlineData(1, "title", 2.0, true)] //Valid MovieId, Valid title, Valid averageScore
        public void Movie_ForValues_ValidatesCorrectly(int movieId, string title, decimal averageScore, bool expectedResult)
        {
            // Given
            var movie = new Movie
            {
                MovieId = movieId,
                Title = title,
                Genere = "",
                AverageScore = averageScore
            };
            var validationContext = new ValidationContext(movie);
            
            // When
            var result = Validator.TryValidateObject(movie, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}
