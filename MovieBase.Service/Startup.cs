﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieBase.Domain.Contexts;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Contracts.Repositories;
using MovieBase.Domain.Contracts.Services;
using MovieBase.Domain.Repositories;
using MovieBase.Domain.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace MovieBase.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddDbContext<MovieDbContext>(o => o.UseInMemoryDatabase("MovieDb"));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddDbContext<UserDbContext>(o => o.UseInMemoryDatabase("UserDb"));

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "Movie Base Service", Version = "v1" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("../swagger/v1/swagger.json", "Movie Base Service"));
        }
    }
}
