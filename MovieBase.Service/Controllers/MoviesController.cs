﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Contracts.Services;
using MovieBase.Domain.Models;

namespace MovieBase.Service.Controllers
{
    [Route("Movies")]
    [ProducesResponseType(500)]
    public class MoviesController : Controller
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet("add/{title}/{genere}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddMovie(string title, string genere)
        {
            NewMovieModel movie = new NewMovieModel { Title = title, Genere = genere };
            if (!TryValidateModel(movie))
                return BadRequest();

            await _movieService.AddMovieAsync(movie);

            return Ok();
        }

        [HttpGet("all")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAllMovies()
        {
            var movies = await _movieService.GetMoviesAsync();
            return Ok(movies);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);
            return Ok(movie);
        }

        [HttpGet("delete/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            await _movieService.DeleteMovieAsync(id);
            return Ok();
        }
    }
}